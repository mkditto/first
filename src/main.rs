extern crate amethyst;

use amethyst::{
    prelude::*,
    renderer::{Event, DisplayConfig, DrawFlat, Pipeline, PosNormTex, RenderBundle, Stage, VirtualKeyCode, KeyboardInput, WindowEvent, WindowMessages, ElementState},
    utils::application_root_dir,
};


struct Example {
    fullscreen: bool,
}

impl SimpleState for Example {
    fn handle_event(&mut self, _data: StateData<'_, GameData<'_, '_>>, event: StateEvent) -> SimpleTrans {
        if let StateEvent::Window(event) = &event {
            match event {
                Event::WindowEvent { event, .. } => {
                    match event {
                        WindowEvent::KeyboardInput { input : KeyboardInput { state: ElementState::Pressed, virtual_keycode : Some(key), .. }, ..} => {
                            match key {
                                VirtualKeyCode::Escape => Trans::Quit,
                                VirtualKeyCode::F11 => {
                                    let tmp = self.fullscreen;
                                    let mut wcmd = _data.world.write_resource::<WindowMessages>();

                                    wcmd.send_command(move |w| {
                                        let id = if !tmp {
                                            Some(w.get_current_monitor())
                                        } else {
                                            None
                                        };

                                        w.set_fullscreen(id);
                                    });

                                    self.fullscreen = !self.fullscreen;
                                    Trans::None
                                },
                                _ => { 
                                    println!("{:?} was pressed", key); 
                                    Trans::None 
                                },
                            }
                        },
                        WindowEvent::CloseRequested => Trans::Quit,
                        _ => Trans::None,
                    }
                },
                _ => Trans::None,
            }
        } else {
            Trans::None
        }
    }
}


fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = format!(
        "{}/resources/display_config.ron",
        application_root_dir()
    );

    let config = DisplayConfig::load(&path);
    let pipe = Pipeline::build().with_stage(
            Stage::with_backbuffer()
                .clear_target([0.00196, 0.23726, 0.21765, 1.0], 1.0)
                .with_pass(DrawFlat::<PosNormTex>::new()),
        );

    let game_data = GameDataBuilder::default().with_bundle(
            RenderBundle::new(pipe, Some(config))
        )?;

    let mut game = Application::new("./", Example { fullscreen : false }, game_data)?;

    game.run();
    
    Ok(())
}
